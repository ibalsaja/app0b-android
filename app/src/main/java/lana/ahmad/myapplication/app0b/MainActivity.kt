package lana.ahmad.myapplication.app0b

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class MainActivity : AppCompatActivity(),View.OnClickListener {
    override fun onClick(v: View?){
        when(v?.id){
            R.id.imgUpload->{
                requestPermissions()

            }
            R.id.btnInsert->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete->{
                queryInsertUpdateDelete("delete")
            }
            R.id.btnCamera->{

            }
        }


    }

    val url = "http://192.168.1.3/sepatu/show_data.php"
    val url2 =  "http://192.168.1.3/sepatu/get_jenis_sepatu.php"
    val url3 =  "http://192.168.1.3/sepatu/query_crud.php"
    val url4 = "http://192.168.1.3/sepatu/upload.php"

    var imStr = ""
    var namafile = ""
    var fileUri = Uri.parse("")
    var pilihJenis = ""
    var daftarSepatu = mutableListOf<HashMap<String,String>>()
    var daftarJenis = mutableListOf<String>()
    lateinit var mediaHelper : MediaHelper
    lateinit var sepatuAdapter = AdapterDataSepatu
    lateinit var builder : AlertDialog.Builder
    lateinit var jenisAdapter : ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sepatuAdapter = AdapterDataSepatu(daftarSepatu,this)
        lsSptu.layoutManager = LinearLayoutManager(this)
        lsSptu.adapter = sepatuAdapter
        jenisAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarJenis)
        spJenis.adapter = jenisAdapter
        spJenis.onItemSelectedListener = itemSelected
        imgUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()

        }
        mediaHelper = MediaHelper()
    }

    fun uploadFile(){
        val request = object : StringRequest(Method.POST,url4,
            Response.Listener { response ->
                val jsonObject = JSONObject(response);
                val kode = jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(this,"Upload foto sukses", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this,"Upload foto gagal",Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"koneksi gagal upload file",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("imstr",imStr)
                hm.put("namafile", namafile)
                return hm
            }
        }
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }


    val btnuploadImg = DialogInterface.OnClickListener { dialog, which ->
        uploadFile()
    }
    fun requestPermissions() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }


    override fun onStart(){
        super.onStart()
        showDataSepatu("")
        getJenisSepatu()
    }
val itemSelected  = object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spJenis.setSelection(0)
        pilihJenis = daftarJenis.get(0)
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        pilihJenis = daftarJenis.get(position)
    }

}
    fun showDataSepatu(namaSepatu: String){
        val request =object : StringRequest(
            Method.POST,url,
            Response.Listener { response ->
                daftarSepatu.clear()
                Toast.makeText(this,"koneksi ke server (data mhs)",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var sepatu = HashMap<String,String>()
                    sepatu.put("merk_sepatu",jsonObject.getString("merk_sepatu"))
                    sepatu.put("nama_sepatu",jsonObject.getString("nama_sepatu"))
                    sepatu.put("jenis_sepatu",jsonObject.getString("jenis_sepatu"))
                    sepatu.put("url",jsonObject.getString("url"))
                    daftarJenis.add(sepatu.toString())
                }
                sepatuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama",namaSepatu)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode== Activity.RESULT_OK){
            /*if (requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,imgUpload)
            }*/
            if(requestCode==mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(fileUri,imgUpload)
                namafile = mediaHelper.getMyFileName()
                uploadFile()
            }
        }
    }

    fun getJenisSepatu(){
        val request = StringRequest(Request.Method.POST,url2,
            Response.Listener { response ->
                daftarJenis.clear()
                Toast.makeText(this,"koneksi ke server (jenis_sepatu)",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarJenis.add(jsonObject.getString("jenis_sepatu"))
                }
                jenisAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error= jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_SHORT).show()
                    showDataSepatu("")

                }
                else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung dengan server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("merk_sepatu",edMerk.text.toString())
                        hm.put("nama_sepatu",edNamaSptu.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",nmFile)
                        hm.put("jenis_sepatu",pilihJenis)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("merk_sepatu",edMerk.text.toString())
                        hm.put("nama_sepatu",edNamaSptu.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",nmFile)
                        hm.put("jenis_sepatu",pilihJenis)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("merk_sepatu",edMerk.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


}
