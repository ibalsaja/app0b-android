package lana.ahmad.myapplication.app0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataSepatu(val dataSep : List<HashMap<String,String>>, val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterDataSepatu.HolderDataSepatu>(){
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataSepatu.HolderDataSepatu {
            val v = LayoutInflater.from(p0.context).inflate(R.layout.row_sepatu,p0,false)
            return  HolderDataSepatu(v)
        }

        override fun getItemCount(): Int {
            return dataSep.size
        }

        override fun onBindViewHolder(p0: AdapterDataSepatu.HolderDataSepatu, p1: Int) {
            val data = dataSep.get(p1)
            p0.txtMerk.setText(data.get("merk"))
            p0.txtNama.setText(data.get("nama"))
            p0.txtJenis.setText(data.get("jenis"))
            if(p1.rem(2) == 0) p0.clayout.setBackgroundColor(
                Color.rgb(230,245,240))
            else p0.clayout.setBackgroundColor(Color.rgb(255,255,245))

            p0.clayout.setOnClickListener({

            })
            if(!data.get("url").equals("")) {
                Picasso.get().load(data.get("url")).into(p0.photo)
            }
        }

        class HolderDataSepatu(v: View) : RecyclerView.ViewHolder(v){
            val txtMerk = v.findViewById<TextView>(R.id.txMerk)
            val txtNama = v.findViewById<TextView>(R.id.txNama)
            val txtJenis = v.findViewById<TextView>(R.id.txJenis)
            val photo = v.findViewById<ImageView>(R.id.imgSepatu)
            val clayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
        }
    }